

#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	sqlite3_stmt *stmt;
	int rc;

	cout << "select * from accounts where Buyer_id = ? and accounts.id.?.balance => cars.id.?.price and cars.id.?.available = 1" << endl;
	clearTable();

	rc = sqlite3_exec(db, "select * from accounts where Buyer_id = ? and accounts.id.?.balance => cars.id.?.price and cars.id.?.available = 1", callback, 0, &zErrMsg);
	
	
	if (rc == SQLITE_OK) 
	{
		// bind the value 
		sqlite3_bind_int(stmt, 1, buyerid);
		sqlite3_bind_int(stmt, 2, buyerid);
		sqlite3_bind_int(stmt, 3, carid);
		sqlite3_bind_int(stmt, 4, carid);
		// commit 
		sqlite3_step(stmt);
		sqlite3_finalize(stmt);

		printTable();
	}
	
	else if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	

	system("Pause");
	system("CLS");

	cout << "update accounts set accounts.id.?.balance = accounts.id.?.balance - cars.id.?.price and cars.id.?.available = 0" << endl;

	rc = sqlite3_exec(db, "update accounts set accounts.id.?.balance = accounts.id.?.balance - cars.id.?.price and cars.id.?.available = 0", callback, 0, &zErrMsg);

	if (rc == SQLITE_OK) 
	{
		// bind the value 
		sqlite3_bind_int(stmt, 1, buyerid);
		sqlite3_bind_int(stmt, 2, buyerid);
		sqlite3_bind_int(stmt, 3, carid);
		sqlite3_bind_int(stmt, 4, carid);
		// commit 
		sqlite3_step(stmt);
		sqlite3_finalize(stmt);

		printTable();
	}

	else if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	system("Pause");
	system("CLS");

	return true;
}


bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	sqlite3_stmt *stmt;
	int rc;

	cout << "update accounts set balance = balance + ? where Buyer_id = ?" << endl;

	rc = sqlite3_exec(db, "update accounts set balance = balance + ? where Buyer_id = ?", callback, 0, &zErrMsg);

	if (rc == SQLITE_OK)
	{
		// bind the value 
		
		sqlite3_bind_int(stmt, 1, amount);
		sqlite3_bind_int(stmt, 2, to);
		
		// commit 
		sqlite3_step(stmt);
		sqlite3_finalize(stmt);

		printTable();
	}

	else if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	system("Pause");
	system("CLS");

	cout << "update accounts set balance = balance - ? where Buyer_id = ?" << endl;

	rc = sqlite3_exec(db, "update accounts set balance = balance - ? where Buyer_id = ?", callback, 0, &zErrMsg);

	if (rc == SQLITE_OK)
	{
		// bind the value 

		sqlite3_bind_int(stmt, 1, amount);
		sqlite3_bind_int(stmt, 2, from);

		// commit 
		sqlite3_step(stmt);
		sqlite3_finalize(stmt);

		printTable();
	}

	else if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	system("Pause");
	system("CLS");

	return true;

}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;
	bool check = true;
	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");

	check = carPurchase(2, 4, db, zErrMsg);
	check = carPurchase(1, 1, db, zErrMsg);
	check = carPurchase(4, 3, db, zErrMsg);

	system("pause");

	sqlite3_close(db);

}






